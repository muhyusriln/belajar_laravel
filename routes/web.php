<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/halo', function () {
    return "Halo, Selamat datang di perusahaan StartDev oleh CEO Muhammad Yusril Nugraha Putra";
});

Route::get('/hallo/{nama}', function ($nama) {
    return "Halo $nama";
});

Route::get('/blog', function () {
    $nama = 'Muhammad Yusril';
    return view('blog', ['nama' => $nama]);
});

// Cara menggunakan Controller di Laravel
// Membuat Route untuk menghubungkan controller yang dituju
Route::get('/biodata', 'FormController@index');
// Membuat Route form sederhana
Route::get('/formulir', 'FormController@formulir');
// Membuat Route form untuk mengirim data
Route::post('/formulir/proses', 'FormController@proses');

// Cara menangkap data melalui URI
// Membuat Route yang mengarah ke controller yang dituju
Route::get('/test/{nama}', 'CobaController@index');